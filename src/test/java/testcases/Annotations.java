package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.yalla.selenium.api.base.SeleniumBase;

//import com.yalla.selenium.api.base.SeleniumBase;

import utils.Datalibrary;
import utils.Datalibrary2;

public class Annotations extends SeleniumBase{
//public class Annotations {
	
	public String t;
	@DataProvider(name = "projectday")
	public Object[][] fetchData() throws Exception {
		//return Datalibrary2.readExceldata(t);
		return Datalibrary.readExceldata(excelname);
	}	
 @Parameters({"url","username","password"})
  @BeforeMethod
  public void beforeMethod(String url,String username,String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
  }

  @AfterMethod
  public void afterMethod() {
	close();
  }

  }
