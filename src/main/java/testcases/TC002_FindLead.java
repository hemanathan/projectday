package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC002_FindLead extends Annotations {
	
	@BeforeTest
	public void setData()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="hem";
		category="Smoke";
		excelname = "Datasheet";
	}

	@Test(dataProvider="fetchData")
public void findLeadScenario(String firstName,String CompanyName)
{
		
		
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), firstName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		
		
		
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("xpath", "//a[text()='Edit']"));
		clearAndType(locateElement("xpath", "//input[@id='updateLeadForm_companyName']"), CompanyName);
		click(locateElement("xpath", "//input[@value='Update']"));
		
		
		verifyExactText(locateElement("xpath", "//span[@id='viewLead_companyName_sp']"), CompanyName);
	
			
}

}
