package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.Datalibrary;

public class Merge_Lead extends Annotations{
	@BeforeTest
	public void setdata()
	{
		testcaseName="mergeLead";
		testcaseDec="merge a Lead";
		author="hem";
		category="sanity";
		excelname = "Datasheet_merge";

	}
	@DataProvider(name="createdata")
	public Object[][] fetchdata() throws Exception{
		return Datalibrary.readExceldata(excelname);
	}
	@Test(dataProvider = "createdata")
	public void MergeLead() throws Throwable {
		WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
		click(eleCrmSfa);
		reportStep(driver, "CRM cliked");
		WebElement elecreatelead = locateElement("link", "Create Lead");
		click(elecreatelead);
		reportStep(driver, "create lead ");
		WebElement eleMergelead = locateElement("link", "Merge Leads");
		click(eleMergelead);
		reportStep(driver, "merge lead ");
		System.out.println(driver.getTitle());
		WebElement eleFrom = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFrom);
		reportStep(driver, "");
		
		Set<String> allwins= driver.getWindowHandles();
		List<String> lst= new ArrayList<>();
		lst.addAll(allwins);
		driver.switchTo().window(lst.get(1));
		WebElement eleLeadList = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(eleLeadList);
		driver.switchTo().window(lst.get(0));
		WebElement eleTo = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleTo);
		driver.switchTo().window(lst.get(1));
		Thread.sleep(2000);
		WebElement eleLeadList2 = locateElement("xpath", "(//a[@class='linktext'])[1]");
		click(eleLeadList2);
		//	driver.switchTo().window(lst.get(0));
		driver.switchTo().defaultContent();
		WebElement eleMerge = locateElement("class", " ext-safari");
		click(eleMerge);

	}
} 