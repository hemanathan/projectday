package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporting {
	@Test
	public void extentReporting() throws IOException {
		
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./report/result.html");
		reporter.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		ExtentTest test = extent.createTest("create", "creating test lead");
		test.assignAuthor("hem");
		test.fail("pass", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snaps.png").build());
		test.pass("username", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/pass.png").build());
				extent.flush();
	}

}
