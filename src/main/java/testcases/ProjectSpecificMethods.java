package testcases;

import org.openqa.selenium.WebElement;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.cli.Main;

public class ProjectSpecificMethods extends SeleniumBase{

	
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement username = locateElement("id", "username");
	clearAndType(username, "DemoSalesManager");
	WebElement password = locateElement("id", "password");
	clearAndType(password, "crmsfa");
	WebElement login = locateElement("class", "decorativeSubmit");
	click(login);
	}
	
	public void logout()
	{
		close();
		
	}
	
	

	public void test() {	
		startApp("chrome", "www.google.com");
		
	}
}


