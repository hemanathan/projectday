package testcases;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

import utils.Datalibrary;

public class CreateLead_annotations2 extends Annotations{
	@BeforeTest
	public void setdata()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="hem";
		category="Smoke";
		excelname = "Datasheet_merge_parallel";

	}
	@DataProvider(name="mergedata")
	public Object[][] fetchdata() throws Exception{
		return Datalibrary.readExceldata(excelname);
	}
	
	@Test(dataProvider = "mergedata")
	public void cLead(String cname, String fname,String lname) throws Exception {
	click(locateElement("link","CRM/SFA"));
	reportStep(driver, "clicked CRM/SFA");
	WebElement createlead = locateElement("link","Create Lead");
	click(createlead);
	reportStep(driver, "createlead clicked");
	Thread.sleep(4000);
	WebElement companyName = locateElement("id", "createLeadForm_companyName");
	Thread.sleep(3000);
	clearAndType(companyName, cname);
	reportStep(driver, "companyName typed");
	
	WebElement eleFNAME = locateElement("id", "createLeadForm_firstName");
	clearAndType(eleFNAME, fname);
	WebElement eleLNAME = locateElement("id", "createLeadForm_lastName");
	clearAndType(eleLNAME, lname);
	WebElement source = locateElement("name", "dataSourceId") ;
	selectDropDownUsingText(source, "Direct Mail");
	reportStep(driver, "dropdown selected");
	WebElement createLeadBtn = locateElement("xpath", "//input[@class='smallSubmit']");
	createLeadBtn.click();
	Thread.sleep(3000); 
	reportStep(driver, "");
	
	}
	


}
