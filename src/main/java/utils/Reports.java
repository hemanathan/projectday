package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports{
	
	 static ExtentHtmlReporter htmlReporter;
	 static   ExtentReports extent;
	 static   ExtentTest test;
	     
	public String testcaseName, testcaseDec, author, category,details  ;
	public  String  excelname;
	
	@BeforeSuite
	public void startReport() {
		Date d=new Date();
		SimpleDateFormat df=new SimpleDateFormat("DDMMYYHHMMSS");
		String name=df.format(d);
		

		   htmlReporter = new ExtentHtmlReporter("./report/HTMLReport"+name+".html");
		   htmlReporter.setAppendExisting(true);
	        extent = new ExtentReports();
	        extent.attachReporter(htmlReporter);

	}
    @BeforeMethod
	public void report() throws IOException {
      //  test = extent.createTest("logsGeneration");
	   
	    
	}
    @BeforeClass

	public void report1() throws IOException {

		test = extent.createTest(testcaseName, testcaseDec);

	    test.assignAuthor(author);

	    test.assignCategory(category);  

	}
    
    public void reportStep(WebDriver driver, String msg) throws IOException {
    	// test.log(Status.INFO,"");
         String sc= snaps(driver);
         test.pass(msg, MediaEntityBuilder.createScreenCaptureFromPath(sc).build());
         
        
 		
 		
 	
    }
    
public String snaps(WebDriver driver) {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String name=source.getName();
		File des = new File("./snaps/"+name);
		try {
			FileUtils.copyFile(source, des);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		return "../snaps/"+name;
	

}
    @AfterSuite
    public void stopReport() {
    	extent.flush();
    }
}