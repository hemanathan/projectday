package utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Datalibrary {

	public static  Object[][] readExceldata(String excelname) throws Exception {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./Datasheet/"+excelname+".xlsx");
	
		XSSFSheet sheet = wbook.getSheetAt(0);
		
		
	//	XSSFSheet sheet = wbook.getSheet("createlead");
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("last row is :"+lastRowNum);
		int lastColNum = sheet.getRow(0).getLastCellNum();
		System.out.println("last column is :"+lastColNum);
		
		Object[][] data = new Object[lastRowNum][lastColNum];
		
		
		for(int i=1;i<=lastRowNum;i++) {
		XSSFRow row = sheet.getRow(i);			
		for(int j=0;j<lastColNum;j++)
		{
		XSSFCell cell = row.getCell(j);
		String ColumnValue = cell.getStringCellValue();
		data[i-1][j] = cell.getStringCellValue();
		System.out.println(ColumnValue);
		}
		
		}
		wbook.close();
		return data;

	}

}
