package com.yalla.selenium.api.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testcases.Annotations;

public class TC004_DuplicateLead extends Annotations {
	@BeforeClass
	public void setData()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="hem";
		category="Smoke";
		excelname = "Datasheet";
	}
	@Test(dataProvider="fetchData")
	
public void duplicateLeadcreation(String email)
{

		
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Email']"));
		
		clearAndType(locateElement("xpath", "//input[@name='emailAddress']"), email);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		
		String firstLeadName = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a").getText();
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("link", "Duplicate Lead"));
		verifyTitle("Duplicate Lead | opentaps CRM");
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		verifyExactText(locateElement("id", "viewLead_firstName_sp"), firstLeadName);
		
			
}

}
