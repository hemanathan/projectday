package com.yalla.selenium.api.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testcases.Annotations;

public class TC005_DeleteLead extends Annotations {
	@BeforeClass
	public void setData()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="hem";
		category="Smoke";
		excelname = "Datasheet";
	}
	@Test(dataProvider="fetchData")
	
public void findLeadScenario(String firstName)
{
		
		
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		
		clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), firstName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		
		 String DeletedLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		 click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		 click(locateElement("xpath", "//a[text()='Delete']"));
		    

		
		click(locateElement("link", "Find Leads"));
	    clearAndType(locateElement("xpath", "//input[@name='id']"), DeletedLead);
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	    verifyExactText(locateElement("class","x-paging-info"), "No records to display");
		
		
		
		
			
}

}
