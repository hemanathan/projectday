package com.yalla.selenium.api.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import testcases.Annotations;

public class TC003_MergeLead extends Annotations {
	//@BeforeClass
	@BeforeTest(groups="common")
	public void setData()
	{
		testcaseName="CreateLead";
		testcaseDec="Creating a Lead";
		author="hem";
		category="Smoke";
		excelname = "Datasheet";
	}
	//@Ignore
	@Test(dataProvider="fetchData")
	//@Test(groups="regression")
public void mergeLead(String lead1,String lead2) throws InterruptedException
{
		
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Merge Leads"));
		
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]"));
		switchToWindow(1);
		clearAndType(locateElement("name", "id"), lead1);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		String lead1text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		
		
		clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		//switchToWindow("Find Leads");
		
		
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]"));
		switchToWindow(1);
		//switchToWindow("Merge Leads | opentaps CRM");
		clearAndType(locateElement("name", "id"), lead2);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		//switchToWindow("Find Leads");	
		clickWithNoSnap(locateElement("link", "Merge"));
		
	
		acceptAlert();
		click(locateElement("link", "Find Leads"));
		clearAndType(locateElement("name", "id"), lead1text);
		
		verifyExactText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");
	
		
		
		
		
}

}
