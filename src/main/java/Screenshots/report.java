package Screenshots;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.yalla.selenium.api.base.SeleniumBase;

public class report extends screenshot{
	ExtentHtmlReporter path;
	ExtentReports extent;
	ExtentTest test;
	String s ;


	public void reporting_new (String message) throws Exception {
		try {
			path = new ExtentHtmlReporter("./report/result.html");
			extent = new ExtentReports();
			extent.attachReporter(path);
			test = extent.createTest("'");
			test.pass("pass", MediaEntityBuilder.createScreenCaptureFromPath(s).build());
			String s=snaps();
		
		} catch (IOException e) {
			test.fail("pass", MediaEntityBuilder.createScreenCaptureFromPath(s).build());
			s = snapshot(message);
			e.printStackTrace();
		}
		extent.flush();
	}

	public void reporting_img (String message) throws Exception {
		try {
			path = new ExtentHtmlReporter("./report/result.html");
			extent = new ExtentReports();
			extent.attachReporter(path);
			test = extent.createTest("'");
			s = snapshot(message);
			test.pass("info", MediaEntityBuilder.createScreenCaptureFromPath(s).build());
		} catch (IOException e) {

		}
	}
	public void reporting_img_fail (String message) throws Exception {
		try {
			path = new ExtentHtmlReporter("./report/result.html");
			extent = new ExtentReports();
			extent.attachReporter(path);
			test = extent.createTest("'");
			s = snapshot(message);
			test.fail("info", MediaEntityBuilder.createScreenCaptureFromPath(s).build());
		} catch (IOException e) {

		}
	}

	public void reporting_image (String message) throws Exception {
		try {
			path = new ExtentHtmlReporter("./report/result.html");
			
			String sc= snaps();
			ExtentTest sc2=test.addScreenCaptureFromPath(sc);
			test.log(Status.PASS, "Screenshjot"+sc2);
		
			
		} catch (IOException e) {

		}
	}
}
