package Screenshots;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testcases.Annotations;


public class Sample extends report{

	
	
	@Test
	public void samplereport() throws Exception
	{
		
		
		
	//driver.get("http://leaftaps.com/opentaps");
		click(locateElement("link","CRM/SFA"));
		WebElement createlead = locateElement("link","Create Lead");
		click(createlead);
		reporting_image("clicked");
		Thread.sleep(4000);
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyName, "abbb");
		reporting_image("entered");
		WebElement source = locateElement("name", "dataSourceId") ;
		selectDropDownUsingText(source, "Direct Mail");
		reporting_image("dropdown");
		Thread.sleep(3000);
		
		//close();
		
	}
	

}
