package Screenshots;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.yalla.selenium.api.base.SeleniumBase;

public class screenshot extends SeleniumBase{

	public String snaps() {
		
	//	startApp("chrome", "http://leaftaps.com/opentaps");
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String name=source.getName();
		File des = new File("./snaps/"+name+".png");
		try {
			FileUtils.copyFile(source, des);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		return "./snaps/"+name+".png";
	}

}
