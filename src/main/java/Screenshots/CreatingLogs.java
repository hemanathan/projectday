package Screenshots;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
 
public class CreatingLogs
{
    ExtentHtmlReporter htmlReporter;
    ExtentReports extent;
    ExtentTest test;
     
    
    public void config()
    {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/MyOwnReport.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        test = extent.createTest("logsGeneration");
    }
     
    
    public void logsGeneration()
    {
        test = extent.createTest("logsGeneration");
        test.log(Status.INFO,"createTest() method will return the ExtentTest object");
        test.log(Status.INFO, "I am in actual Test");
        test.log(Status.INFO, "We can write the actual test logic in this Test");
         
        // Using Labels
        test.log(Status.INFO, MarkupHelper.createLabel("*************** Using Labels ***************", ExtentColor.RED));
        test.log(Status.INFO, MarkupHelper.createLabel("This is Test Logger 1", ExtentColor.BLUE));
        test.log(Status.INFO, MarkupHelper.createLabel("This is Test Logger 2", ExtentColor.BLUE));
        test.log(Status.INFO, MarkupHelper.createLabel("This is Test Logger 3", ExtentColor.BLUE));
        test.log(Status.INFO, MarkupHelper.createLabel("This is Test Logger 4", ExtentColor.BLUE));
    }
    public void logsGeneration_screenshot(WebDriver driver) throws IOException
    {
        
        test.log(Status.INFO,"createTest() method will return the ExtentTest object");
        String sc= snaps(driver);
		ExtentTest sc2=test.addScreenCaptureFromPath(sc);
		test.log(Status.INFO, "details");
	
     }
    
    public void tearDown()
    {
        extent.flush();
    }
public String snaps(WebDriver driver) {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String name=source.getName();
		File des = new File("./snaps/"+name);
		try {
			FileUtils.copyFile(source, des);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		return "../snaps/"+name;
	

}
}