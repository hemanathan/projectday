package testing_report;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Screenshots.CreatingLogs;

public class Test_report {

	public static void main(String[] args) throws IOException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.google.com");
		
		/*snappps sn = new snappps();
		sn.createReport();
		sn.addscreebcapture(driver);*/
		CreatingLogs l=new CreatingLogs();
		l.config();
		l.logsGeneration_screenshot(driver);
		l.tearDown();

		
	}

}
