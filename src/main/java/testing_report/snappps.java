package testing_report;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class snappps {
	ExtentHtmlReporter path;
	static ExtentReports extent;
	static ExtentTest test;
	String s ;

	public String snaps(WebDriver driver) {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String name=source.getName();
		File des = new File("./snaps/"+name+".png");
		try {
			FileUtils.copyFile(source, des);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		return "./snaps/"+name+".png";
	

}
	public void createReport()
	{
		path = new ExtentHtmlReporter("./report/result.html");
		extent = new ExtentReports();
		extent.attachReporter(path);
		test = extent.createTest("Hema");
		
		
	
	
	
	}
	
	public void addscreebcapture(WebDriver driver)
	{
		try {
			//path = new ExtentHtmlReporter("./report/result.html");
			
			String sc= snaps(driver);
			ExtentTest sc2=test.addScreenCaptureFromPath(sc);
			test.log(Status.INFO, "details");
		
			
		} catch (IOException e) {
         e.printStackTrace();
		}
	}
}


